// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// The main menu.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_Widget',
	'dijit/_TemplatedMixin',
	'dojo/dom-class',
	'../ViewWeek/ViewWeek',
	'../MakeSchedule/MakeSchedule',
//wrap
	'../StudentTable/StudentTable',
	'../MakeTemplate/MakeTemplate',
	'../RoomTable/blankTemplate',
	'../RoomTable/timeUtils',
	'dojo/date/locale',
	'dojo/text!./Main.html'
],
function (dojoDeclare, dojoConfig, dijitWidget, dijitTemplatedMixin, dojoDomClass, ViewWeek, MakeSchedule,
StudentTable, MakeTemplate, blankTemplate, timeUtils, dojoDateLocale, templateString) {

var myAlert = function (msg) {
	msg = 'Main.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

// Wrapped the work space into a container but I think I will need some metadata down the road.
var workSpaceContainer = {
	workSpace : {
		// Map of ISO date string to scheduled data.
		scheduledDays : {},

		students : [],

		// Map of template name to template object.
		templates : {
			'Normal' : blankTemplate
		}
	}
};

return dojoDeclare([dijitWidget, dijitTemplatedMixin], {
	// @Override
	templateString : templateString,
	mainNode : null,

	// @Override
	postCreate : function () {
		this.makeSchedule = new MakeSchedule({}).placeAt(this.domNode);
		this.makeSchedule.startup();

		this.studentTable = new StudentTable({
			students : workSpaceContainer.workSpace.students
		}).placeAt(this.domNode);
		this.studentTable.startup();

		this.makeTemplate = new MakeTemplate({
			templatesData : workSpaceContainer.workSpace.templates
		}).placeAt(this.domNode);
		this.makeTemplate.startup();

		this.hideAll();
	},

	hideAll : function () {
		this.hideMain();
		if (this.viewWeek) this.viewWeek.hide();
		this.makeSchedule.hide();
		this.studentTable.hide();
		this.makeTemplate.hide();
	},

	// @Override
	startup : function () {
		this.inherited(arguments);

		var _this = this;

		// See https://developer.mozilla.org/en-US/docs/Web/Events/hashchange
		window.onhashchange = function (hashChangeEvent) {
			try {
				_this.navigate(window.location.hash);
			} catch (error) {
				myAlert('window.onhashchange error: ' + error);
			}
		};

		// Try to figure out the best thing to do with the location hash on a page reload.
		// See https://developer.mozilla.org/en-US/docs/Web/Events/popstate
		window.onhashchange();
	},

	navigate : function (locationHash) {
		this.hideAll();
		window.scrollTo(0,0);

		if (0 == locationHash.indexOf('#viewWeek')) {
			// Format of hash is #viewWeek.ISO-DATE
//		case '#viewWeek':
			var strDate = locationHash.split('.')[1];
			strDate = timeUtils.findLastMondayDateStr(strDate);
			var isMakeWidget = false;
			if (this.viewWeek) {
				// If strDate matches the view currently availible then just show.
				// else destroy widget.
				if (strDate != this.viewWeek.strWeekDate) {
					this.viewWeek.destroyRecursive();
					this.viewWeek = null;
					isMakeWidget = true;
				}
			} else {
				isMakeWidget = true;
			}

			if (isMakeWidget) {
				this.viewWeek = new ViewWeek({
					strWeekDate : strDate,
					scheduledDays : workSpaceContainer.workSpace.scheduledDays
				}).placeAt(this.domNode);
				this.viewWeek.startup();
			}

			this.viewWeek.show();
		} else {
			switch (locationHash) {
			case '':
			case '#':
			case '#main':
				this.showMain();
				break;
			case '#scheduleADay':
				this.makeSchedule.show();
				break;
			case '#manageStudents':
				this.studentTable.show();
				break;
			case '#makeATemplate':
				this.makeTemplate.show();
				break;
			default:
				myAlert('Main.navigate - unsupported locationHash : ' + locationHash);
			}
		}
	},

	hideMain : function () {
		dojoDomClass.add(this.mainNode, 'hidden');
		return this;
	},

	showMain : function () {
		dojoDomClass.remove(this.mainNode, 'hidden');
		return this;
	},

	onClickViewWeek : function () {
		var strDate = dojoDateLocale.format(new Date(), { selector: 'date', datePattern : 'yyyy-MM-dd' });
		window.location.hash = '#viewWeek.' + strDate;
	},

	onClickScheduleADay : function () {
		window.location.hash = '#scheduleADay';
	},

	onClickManageStudents : function () {
		window.location.hash = '#manageStudents';
	},

	onClickMakeATemplate : function () {
		window.location.hash = '#makeATemplate';
	}
});
});