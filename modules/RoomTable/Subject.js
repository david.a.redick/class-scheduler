// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A display of subjects rooms and block times for teachers.
// This is the template of buckets that students will be filled into.
define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dojox/dtl/_Templated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'dojo/dom-attr',
	'./timeUtils',
	'../Modal/ModalEditClassTimeBlock',
	'./blankTemplate',
	'dojo/text!./Subject.html'
],
function (dojoDeclare, dijitWidgetBase, dojoxDjangoTemplated, dojoxDjangoTagLogic, dojoDomClass,
 dojoDomAttr, timeUtils, ModalEditClassTimeBlock, blankTemplate, templateString) {

var myAlert = function (msg) {
	msg = 'Subject.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

function toggleRoomNameEdit(roomNameReadOnly, roomData) {
	var roomNameEdit = roomNameReadOnly.parentNode.children[1];
	var roomNameInput = roomNameEdit.children[0];
	dojoDomClass.toggle(roomNameReadOnly, 'hidden');
	dojoDomClass.toggle(roomNameEdit, 'hidden');

	if (!dojoDomClass.contains(roomNameEdit, 'hidden')) {
		roomNameInput.focus();
	}

	if (!roomNameInput.onkeydown) {
		roomNameInput.onkeydown = function (inEvent) {
			if ('Enter' == inEvent.code) {
				roomData.name = roomNameInput.value;
				roomNameReadOnly.innerHTML = roomData.name;
				toggleRoomNameEdit(roomNameReadOnly, roomData);
			}
		};
	}
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoTemplated], {
	// @Override
	templateString : templateString,

	subjectData : null,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
		var _this = this;
		this.subjectNameInput.onkeydown = function (inEvent) {
			if ('Enter' == inEvent.code) {
				_this.subjectData.subject = _this.subjectNameInput.value;
				_this.subjectNameReadOnly.innerHTML = _this.subjectNameInput.value;
				_this.toggleEditSubject();
			}
		};
	},

	_rebuild : function () {
		myAlert('_rebuild - The controller should have set this.');
	},

	toggleEditSubject : function () {
		dojoDomClass.toggle(this.subjectNameReadOnly, 'hidden');
		dojoDomClass.toggle(this.subjectNameEdit, 'hidden');
		if (!dojoDomClass.contains(this.subjectNameEdit, 'hidden')) {
			this.subjectNameInput.focus();
		}
	},

	onClickEditSubjectName : function () {
		this.toggleEditSubject();
	},

	onClickCancelSubject : function () {
		this.subjectNameInput.value = this.subjectData.subject;
		this.toggleEditSubject();
	},

	onClickEditRoomName : function (e) {
		var roomNameReadOnly = e.currentTarget;
		var index = dojoDomAttr.get(roomNameReadOnly, 'data-class-scheduler-room-index');
		toggleRoomNameEdit(roomNameReadOnly, this.subjectData.rooms[index]);
	},

	onClickAddRoom : function () {
		var blankRoom = JSON.parse(JSON.stringify(blankTemplate[0].rooms[0]));
		this.subjectData.rooms.push(blankRoom);
		this._rebuild();
	},

	onClickTimeLine : function (e) {
		var timeLineNode = e.currentTarget;

		// NOTE:
		// In Firefox e.offsetY is the offset of the click point of the target node, not the currentTarget.
		// Per spec clientY doesn't take into account the window scroll bars
		var offsetY = (e.clientY + window.pageYOffset) - timeLineNode.offsetTop;

		var timeCoords = timeUtils.offsetInPixelsToTime(offsetY);
		var startMins = timeCoords.mins;
		var newClass = timeUtils.normalizeTimeBlock({
			startMins: startMins,
			endMins: startMins + 45
		});

		var index = dojoDomAttr.get(timeLineNode, 'data-class-scheduler-room-index');
		var isSuccess = timeUtils.insertTimeBlock(this.subjectData.rooms[index].timeLine, newClass);
		if (isSuccess) {
			this._rebuild();
		} else {
			console.log('onClickTimeLine - failed');
		}
		e.stopPropagation();
	},

	onClickClass : function (e) {
		var _this = this;
		var timeBlockNode = e.currentTarget;
		var timeLineNode = timeBlockNode.parentNode;
		var indexRoom = dojoDomAttr.get(timeLineNode, 'data-class-scheduler-room-index');
		var indexTimeBlock = dojoDomAttr.get(timeBlockNode, 'data-class-scheduler-time-block-index');

		var modalEditClassTimeBlock = new ModalEditClassTimeBlock({
			timeBlock : this.subjectData.rooms[indexRoom].timeLine[indexTimeBlock],
			onClose : function () {
				modalEditClassTimeBlock.destroyRecursive();
			},
			onClickDelete : function () {
				modalEditClassTimeBlock.destroyRecursive();
			},
			onClickCancel : function () {
				modalEditClassTimeBlock.destroyRecursive();
			},
			onClickSave : function (updatedTimeBlock) {
				// Clone existing time line.
				// remove existing indexTimeBlock
				// try to insert with updatedTimeBlock
				// if success then destroy modal and rebuild subject
				// else alert

				var testTimeLine = JSON.parse(JSON.stringify(_this.subjectData.rooms[indexRoom].timeLine));

				testTimeLine.splice(indexTimeBlock, 1);

				var isSuccess = timeUtils.insertTimeBlock(testTimeLine, updatedTimeBlock, true);
				if (isSuccess) {
					_this.subjectData.rooms[indexRoom].timeLine = testTimeLine;
					_this._rebuild();
					modalEditClassTimeBlock.destroyRecursive();
				} else {
					alert('Cannot save - invalid settings');
				}
			}
		});
		modalEditClassTimeBlock.placeAt(this.domNode);
		modalEditClassTimeBlock.show();

		e.stopPropagation();
	}
});
});