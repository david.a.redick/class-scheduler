// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// Misc utils dealing with time.
define([
	'dojo/date',
	'dojo/date/locale'
],
function (dojoDate, dojoDateLocale) {

var myAlert = function (msg) {
	msg = 'TimeUtils.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

var module = {};

module.findLastMondayDateStr = function (strDate) {
	var date = dojoDateLocale.parse(strDate, { selector: 'date', datePattern : 'yyyy-MM-dd' });
	while (1 != date.getDay()) {
		date = dojoDate.add(date, 'day', -1);
	}
	strDate = dojoDateLocale.format(date, { selector: 'date', datePattern : 'yyyy-MM-dd' });
	return strDate;
};

// '06:00' to 360
module.time24hToMinsFromMidnight = function (t24h) {
	var parts = t24h.split(':');
	var hours = parseInt(parts[0]);
	var mins = parseInt(parts[1]);
	return mins + (60 * hours);
};

// 360 to '06:00'
module.minsFromMidnightToTime24h = function (minsFromMidnight) {
	var hours = parseInt(minsFromMidnight / 60);
	var mins = minsFromMidnight % 60;
	if (hours < 10) {
		hours = '0' + hours;
	}
	if (mins < 10) {
		mins = '0' + mins;
	}
	return hours + ':' + mins;
};

// School starts at 7am
module.SCHOOL_START_TIME = '07:00';
module.SCHOOL_START_MINS = module.time24hToMinsFromMidnight(module.SCHOOL_START_TIME);

// School ends at 6pm
module.SCHOOL_END_TIME = '18:00';
module.SCHOOL_END_MINS = module.time24hToMinsFromMidnight(module.SCHOOL_END_TIME);

// Currently 1:1 scale of pixels and mins.
// That is, 1 pixel == 1 min
module.SCALE = 1;

// Given an offset in pixels, what time is this?
// We draw the schedule to scale and this function must match the ratio of pixels to min in the CSS.
module.offsetInPixelsToTime = function (offsetInPixels) {
	var mins = module.SCHOOL_START_MINS + (module.SCALE * offsetInPixels);

	return {
		time: module.minsFromMidnightToTime24h(mins),
		mins: mins
	};
};

module.lengthMinsToPixels = function (lengthMins) {
	return module.SCALE * lengthMins;
};

// Ensures that the given time block contains everything it needs.
//
// template must contain
// startMins and endMins
// or startTime and endTime
module.normalizeTimeBlock = function (template, isMutate) {
	var result = {};

	if (isMutate) {
		result = template;
	}

	if (template.isSpacer) {
		result.isSpacer = true;
	} else {
		result.teacher = template.teacher || 'TBD';
	}

	result.startMins = template.startMins || module.time24hToMinsFromMidnight(template.startTime);
	result.endMins = template.endMins || module.time24hToMinsFromMidnight(template.endTime);
	result.lengthMins = (result.endMins - result.startMins);
	result.pixels = module.lengthMinsToPixels(result.lengthMins);
	result.startTime = template.startTime || module.minsFromMidnightToTime24h(result.startMins);
	result.endTime = template.endTime || module.minsFromMidnightToTime24h(result.endMins);

	return result;
};

// Given an array of time blocks and a new time block,
// mutate the array by inserting the new time block in chrono order.
//
// If the new time block intersects a 'spacer' time block then
// split the spacer into two.
//
// If there is an intersection of a non spacer time block then return false and there will be no insert.
// Otherwise return true and the array will have the given time block inserted.
//
// time block min required form is:
//  {
// startMins - Number (int)
// endMins - Number (int)
// }
// If spacer then a field isSpacer: true
//
// NOTE: The normalizeSpacer is a bit of hack for when a timeBlock was moved around
// and there are gaps in the spacers.
module.insertTimeBlock = function (arrTimeBlocks, timeBlock, normalizeSpacers) {
	if (normalizeSpacers) {
		arrTimeBlocks.some(function(c,i,a) {
			if (c.isSpacer) {
				if (0 == i) {
					// A spacer at very start should begin at school start.
					c.startMins = module.SCHOOL_START_MINS;
				} else if (a.length == i+1) {
					// A spacer at very end should end when school ends.
					c.endMins = module.SCHOOL_END_MINS;
				} else {
					// A spacer in the middle some where.
					var nop = null;
				}

				if (i-1 > 0) {
					// A spacer with a block before it should start when previous block ends.
					var previous = a[i-1];
					c.startMins = previous.endMins;
				}

				if (i+1 < a.length) {
					// A spacer with a block after it should end when the next block starts.
					var next = a[i+1];
					c.endMins = next.startMins;
				}
			}
			c.endTime = null;
			c.startTime = null;
			module.normalizeTimeBlock(c, true);
		});
	}

	// elements in the form of { index : Number, spacer : time block object }
	var spacersToSplit = [];
	var isFailure = arrTimeBlocks.some(function (tb, index, arr) {
		if (module.isIntersected(tb, timeBlock)) {
			if (tb.isSpacer) {
				spacersToSplit.push({
					spacer: tb,
					index: index
				});
			} else {
				// We hit a normal time block.
				// We can automatically insert this.
				// Make the user manually adjust things or pick another time.
				return true;
			}
		}
		// Keep visiting.
		return false;
	});

	if (isFailure) {
		return false;
	} else {
		// We only hit spacers so split them.
		spacersToSplit.some(function (item, i, arr) {
			var result = module.splitSpacer(item.spacer, timeBlock);
			if ('split' == result.status) {
				arrTimeBlocks.push(result.anotherSpacer);
			} else if ('delete' == result.status) {
				arrTimeBlocks.splice(item.index, 1);
			} else if ('adjusted' == result.status) {
				// Nothing to do.
				var nop = null;
			}
			return false;
		});

		arrTimeBlocks.push(timeBlock);

		arrTimeBlocks.sort(function (a, b) {
			return a.startMins - b.startMins;
		});

		return true;
	}
};

// Assumes that we know that there is a collision.
//
// Result is a object in the form of:
// { status: 'delete' } -- we need to delete the spacer.
// { status: 'split', anotherSpacer: obj } -- the given spacer was split into two and here's the additional one.
// { status: 'adjusted' } -- 
module.splitSpacer = function (spacer, timeBlock) {
	var results = {};

	if (timeBlock.startMins <= spacer.startMins && timeBlock.endMins >= spacer.endMins) {
		// Given time block either matches or eclipses spacer, so kill the spacer
		results.status = 'delete';
	} else if (timeBlock.startMins > spacer.startMins && timeBlock.endMins < spacer.endMins) {
		// The given timeBlock is in the middle of the spacer.
		// So we have to split the spacer in two.
		results.status = 'split';
		results.anotherSpacer = module.normalizeTimeBlock({
			isSpacer : true,
			startMins : timeBlock.endMins,
			endMins : spacer.endMins
		});
		spacer.endMins = timeBlock.startMins;
		spacer.endTime = null;
		module.normalizeTimeBlock(spacer, true);
	} else if (timeBlock.startMins <= spacer.startMins && timeBlock.endMins < spacer.endMins) {
		// Adjust the spacer's start time.
		results.status = 'adjusted';
		spacer.startMins = timeBlock.endMins;
		spacer.startTime = null;
		module.normalizeTimeBlock(spacer, true);
	} else if (timeBlock.endMins >= spacer.endMins && timeBlock.startMins > spacer.startMins) {
		// Adjust the spacer's end time.
		results.status = 'adjusted';
		spacer.endMins = timeBlock.startMins;
		spacer.endTime = null;
		module.normalizeTimeBlock(spacer, true);
	} else {
		myAlert('splitSpacer - failure');
	}

	return results;
};

// Does the two time blocks collid with each other?
module.isIntersected = function (a, b) {
	// If a ends before b starts
	// xor b ends before a starts
	// Then this is NOT a intersection/collision.
	if (a.endMins <= b.startMins || b.endMins <= a.startMins) {
		return false;
	} else {
		return true;
	}
};

return module;
});