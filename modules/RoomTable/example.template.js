// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

define(function () {

// Subjects in order of left to right.
return [
	{
		subject : 'Science',
		rooms : [
			// Rooms in order of left to right.
			{
				name: '1A',
				timeLine : [
					{ teacher: 'Kepler', startTime : '08:00', endTime: '08:45' },
					{ teacher: 'Kepler', startTime : '09:00', endTime: '09:45' },
					{ teacher: 'Kepler', startTime : '13:00', endTime: '13:45' },
					{ teacher: 'Kepler', startTime : '15:00', endTime: '15:45' }
				]
			},
			{
				name: '2A',
				timeLine : [
					{ teacher: 'Wheeler', startTime: '07:00', endTime: '07:45' },
					{ teacher: 'Wheeler', startTime: '08:15', endTime: '09:00' },
					{ teacher: 'Wheeler', startTime: '10:00', endTime: '10:45' },
					{ teacher: 'Wheeler', startTime: '11:45', endTime: '12:30' }
				]
			},
			{
				name: '3A',
				timeLine : [
					{ teacher: 'Audubon', startTime: '08:00', endTime: '08:45' },
					{ teacher: 'Audubon', startTime: '09:00', endTime: '09:45' },
					{ teacher: 'Audubon', startTime: '10:00', endTime: '10:45' },
					{ teacher: 'Audubon', startTime: '11:00', endTime: '11:45' }
				]
			}
		],
	},
	{
		subject : 'Math',
		rooms : [
			{
				name: '1B',
				timeLine: [
					{ teacher: 'Leibniz', startTime: '07:30', endTime: '08:15' },
					{ teacher: 'Leibniz', startTime: '09:45', endTime: '10:30' },
					{ teacher: 'Leibniz', startTime: '11:00', endTime: '11:45' }
				]
			},
			{
				name: '2B',
				timeLine: [
					{ teacher: 'Church', startTime: '09:30', endTime: '10:15' },
					{ teacher: 'Church', startTime: '11:00', endTime: '12:00' },
					{ teacher: 'Church', startTime: '13:00', endTime: '14:00' },
					{ teacher: 'Church', startTime: '15:00', endTime: '16:00' }
				]
			},
			{
				name: '3B',
				timeLine: [
					{ teacher: 'Von Neumann', startTime: '08:00', endTime: '10:00' },
					{ teacher: 'Von Neumann', startTime: '11:00', endTime: '13:00' },
					{ teacher: 'Von Neumann', startTime: '14:00', endTime: '16:00' }
				]
			}
		]
	},
	{
		subject : 'Creativity',
		rooms : [
			{
				name : '1C',
				timeLine : [
					{ teacher: 'Rodin',    startTime: '08:00', endTime: '11:45' },
					{ teacher: 'Van Gogh', startTime: '14:00', endTime: '17:45' }
				]
			}
		]
	}
];
});