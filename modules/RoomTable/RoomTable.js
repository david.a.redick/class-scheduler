// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A display of subjects rooms and block times for teachers.
// This is the template of buckets that students will be filled into.
define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'dojo/_base/lang',
	'./Subject',
	'./timeUtils',
	'./blankTemplate',
	'dojo/text!./RoomTable.html'
],
function (dojoDeclare, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass, dojoLang,
Subject, timeUtils, blankTemplate, templateString) {

var myAlert = function (msg) {
	msg = 'RoomTable.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

var makeSpacerBlock = function (startMins, endMins) {
	return timeUtils.normalizeTimeBlock({
		isSpacer : true,
		startMins : startMins,
		endMins : endMins
	});
};

// Message the templateData with proper times and blocks etc.
var myCleanTemplateData = function (templateData) {
	templateData.minMins = null;
	templateData.maxMins = null;

	// For each subject.
	templateData.some(function (subjectData,i,a) {
		// For each room.
		subjectData.rooms.some(function (roomData, i, a) {
			var timeLine = roomData.timeLine;
			var toInsertIndex = [];

			// For each time block.
			timeLine.some(function (block, i, a) {
				// Compute mins offset from midnight and length based on time.
				block.startMins = timeUtils.time24hToMinsFromMidnight(block.startTime);
				block.endMins = timeUtils.time24hToMinsFromMidnight(block.endTime);
				block.lengthMins = block.endMins - block.startMins;
				
				// Create spacer between but
				// save till after we're done looping to insert it.
				if (i+1 < timeLine.length) {
					var nextBlock = timeLine[i+1];
					if (!(block.isSpacer && nextBlock.isSpacer)) {
						toInsertIndex.push(i+1);
					}
				}
				return false;
			});

			// Insert all the spacers
			// NOTE: timeLine is now a scratch pad for the new data.
			timeLine = [];
			roomData.timeLine.some(function (block, i, a) {
				// If we have a spacer to insert and it's at this index
				// the add it to timeLine before the real block.
				if (toInsertIndex.length > 0) {
					var spacerIndex = toInsertIndex[0];
					if (spacerIndex == i) {
						var spacer = makeSpacerBlock(a[i-1].endMins, block.startMins);
						timeLine.push(spacer);
						toInsertIndex.shift();
					}
				}
				timeLine.push(block);
			});
			roomData.timeLine = timeLine;

			// Check if first block is the overall earliest
			// And if the last block is the overall latest.
			if (timeLine.length > 0) {
				var block = timeLine[0];
				if (null == templateData.minMins || block.startMins < templateData.minMins) {
					templateData.minMins = block.startMins;
				}
				block = timeLine[timeLine.length - 1];
				if (null == templateData.maxMins || block.endMins > templateData.maxMins) {
					templateData.maxMins = block.endMins;
				}
			}

			return false;
		});
		return false;
	});

	// Now that we have overall min and max mins (earliest start and latest end times).
	// Create the start and end spacers.
	// For each subject.
	templateData.some(function (subjectData,i,a) {
		// For each room.
		subjectData.rooms.some(function (roomData, i, a) {
			var timeLine = roomData.timeLine;
			if (timeLine.length > 0) {
				var block = timeLine[0];
				if (templateData.minMins < block.startMins) {
					// push front a spacer
					timeLine.unshift(makeSpacerBlock(templateData.minMins, block.startMins));
				}
				block = timeLine[timeLine.length - 1];
				if (block.endMins < templateData.maxMins) {
					// push back a spacer
					timeLine.push(makeSpacerBlock(block.endMins, templateData.maxMins));
				}
			}
			return false;
		});
		return false;
	});
	

	templateData.isClean = true;
	return templateData;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// Constructor params
	// Schedule template.
	templateData : null,
	showActions : false,

	// @Override
	templateString : templateString,
	nodeActions : null,

	// @Override
	constructor : function (args) {
		/*
		this.templateData = args.templateData;

		if (!this.templateData.isClean) {
			this.templateData = myCleanTemplateData(this.templateData);
		}
		*/
	},

	// @Override
	postCreate : function () {
		var _this = this;

		if (this.showActions) {
			dojoDomClass.toggle(this.nodeActions, 'hidden');
		}

		// Visit and create.
		this.templateData.some(function (c,i,a) {
			_this._createSubjectWidget(c, i, _this.nodeActions);
			return false;
		});
	},

	_createSubjectWidget : function (subjectData, index, placeAtTarget) {
		var _this = this;

		var subjectWidget = new Subject({
			subjectData : subjectData
		});

		subjectWidget._rebuild = function () {
			_this._rebuildSubject(index, subjectWidget);
		};

		subjectWidget.placeAt(placeAtTarget, 'before');
		return subjectWidget;
	},

	// User has altered the time line and the subject widget
	// needs to be recreated.
	_rebuildSubject : function (subjectIndex, oldWidget) {
		this._createSubjectWidget(this.templateData[subjectIndex], subjectIndex, oldWidget);
		oldWidget.destroyRecursive();
	},

	onClickAddSubject : function () {
		var blankSubject = JSON.parse(JSON.stringify(blankTemplate[0]));
		this.templateData.push(blankSubject);
		this._createSubjectWidget(blankSubject, this.templateData.length-1, this.nodeActions);
	}
});
});