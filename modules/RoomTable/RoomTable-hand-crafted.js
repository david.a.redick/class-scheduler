// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A display of subjects rooms and block times for teachers.
// This is the template of buckets that students will be filled into.
define([
	'dojo/_base/declare',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojo/dom-class',
	'dojo/text!./RoomTable.html'
],
function (dojoDeclare, dijitWidgetBase, dojoxDjangoDomTemplated, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'RoomTable.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// Constructor params
	showActions : false,

	// @Override
	templateString : templateString,
	nodeActions : null,


	// @Override
	postCreate : function () {
		if (this.showActions) {
			dojoDomClass.toggle(this.nodeActions, 'hidden');
		}
	},

	onClickAddSubject : function () {
		alert('This will append new subject column to right side with some defaults.');
	},

	onClickAddRoom : function () {
		alert('Toggles the Add Room Here button under each subject.');
	},

	onClick_mock_details : function () {
		alert('This will pop up the details / edit / delete controls');
	}
});
});