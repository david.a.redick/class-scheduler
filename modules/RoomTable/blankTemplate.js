// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

define([
	'./timeUtils'
],
function (timeUtils) {
return [
	{
		subject : 'Untitled',
		rooms : [
			{
				name : 'Untitled',
				timeLine : [
					timeUtils.normalizeTimeBlock({
						isSpacer: true,
						startTime: timeUtils.SCHOOL_START_TIME,
						startMins: timeUtils.SCHOOL_START_MINS,
						endTime: timeUtils.SCHOOL_END_TIME,
						endMins: timeUtils.SCHOOL_END_MINS
					})
				]
			}
		]
	}
];
});