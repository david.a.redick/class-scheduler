// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// Displays high level schedule stats for an entire week.
// This view's main purpose is to allow the user to drill down to a specific day.
define([
	'dojo/_base/declare',
	'dijit/_Widget',
	'dijit/_TemplatedMixin',
	'dojo/dom-class',
	'dijit/Calendar',
	'dojo/text!./MakeSchedule.html'
],
function (dojoDeclare, dijitWidget, dijitTemplatedMixin, dojoDomClass, dijitCalendar, templateString) {

var myAlert = function (msg) {
	msg = 'MakeSchedule.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidget, dijitTemplatedMixin], {
	// Constructor params
	// 'day' xor 'week'
	scheduleFor : 'week',
	currentStep : 'scheduleFor',

	// Template attach points
	// @Override
	templateString : templateString,
	menuItemScheduleFor : null,
	menuItemDailyTemplate : null,
	menuItemStudents : null,
	stepScheduleFor : null,
	radioDay : null,
	radioWeek : null,
	containerPicker : null,
	stepTemplate: null,
	stepStudents : null,

	// @Override
	constructor : function (args) {
		if ('scheduleFor' in args) {
			this.currentStep = 'dailyTemplate';
		}
	},

	// @Override
	postCreate : function () {
		this.picker = new dijitCalendar({}).placeAt(this.containerPicker);

		if ('week' === this.scheduleFor) {
				this.radioDay.checked = false;
				this.radioWeek.checked = true;
		} else if ('day' === this.scheduleFor) {
				this.radioDay.checked = true;
				this.radioWeek.checked = false;
		} else {
			myAlert('unhandled scheduleFor = ' + this.scheduleFor);
		}
		this._changeStep(this.currentStep);
	},

	_changeStep : function (nextStep) {
		dojoDomClass.remove(this.menuItemScheduleFor, 'active');
		dojoDomClass.remove(this.menuItemDailyTemplate, 'active');
		dojoDomClass.remove(this.menuItemStudents, 'active');

		dojoDomClass.add(this.stepScheduleFor, 'hidden');
		dojoDomClass.add(this.stepTemplate, 'hidden');
		dojoDomClass.add(this.stepStudents, 'hidden');

		switch (nextStep) {
			case 'scheduleFor' :
				dojoDomClass.add(this.menuItemScheduleFor, 'active');
				dojoDomClass.remove(this.stepScheduleFor, 'hidden');
				break;
			case 'dailyTemplate' :
				dojoDomClass.add(this.menuItemDailyTemplate, 'active');
				dojoDomClass.remove(this.stepTemplate, 'hidden');
				break;
			case 'students' :
				dojoDomClass.add(this.menuItemStudents, 'active');
				dojoDomClass.remove(this.stepStudents, 'hidden');
				break;
			default :
				myAlert('unhandled nextStep = ' + nextStep);
		}
		this.currentStep = nextStep;
	},

	hide : function () {
		dojoDomClass.add(this.domNode, 'hidden');
		return this;
	},

	show : function () {
		dojoDomClass.remove(this.domNode, 'hidden');
		return this;
	},

	onClickScheduleFor : function () {
		this._changeStep('scheduleFor');
	},
	onClickDailyTemplate : function () {
		this._changeStep('dailyTemplate');
	},
	onClickStudents : function () {
		this._changeStep('students');
	},
	onClickNext : function () {
		var currentToNextStep = {
			'scheduleFor' : 'dailyTemplate',
			'dailyTemplate' : 'students',
			'students' : null
		};
		this._changeStep(currentToNextStep[this.currentStep]);
	}
});
});
