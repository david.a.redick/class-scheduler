// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// Scheduled time for a given day (all or some the blocks may be empty).
define([
	'dojo/_base/declare',
	'dijit/_Widget',
	'dijit/_TemplatedMixin',
	'../RoomTable/RoomTable',
	'dojo/text!./ViewDay.html'
],
function (dojoDeclare, dijitWidget, dijitTemplatedMixin, RoomTable, templateString) {

var myAlert = function (msg) {
	msg = 'ViewDay.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidget, dijitTemplatedMixin], {
	// @Override
	templateString : templateString,

	// @Override
	postCreate : function () {
		this.roomTable = new RoomTable({}).placeAt(this.domNode).startup();
	}
});
});