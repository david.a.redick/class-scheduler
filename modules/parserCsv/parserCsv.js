// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

define([
	'dojo/string',
	'dojo/Deferred'
],
function (dojoString, dojoDeferred) {

var module = {
	// Returns a dojo/Deferred.
	// Where the payload is an array of arrays, where the individual cells contain the csv data.
	readAndParseFile : function (file) {
		var _this = this;
		var deferred = new dojoDeferred();

		// NOTE: Ideally, we should chunk the file but for now just read the entire file.
		this.readSlice(file, 0, file.size).then(function (chunk) {
			var data = _this.parse(chunk);
			deferred.resolve(data);
		});

		return deferred;
	},

	parseLine : function (csvLine) {
		var isQuoted = false;
		var results = [];

		var record = '';
		for (var i = 0; i < csvLine.length; i++) {
			var c = csvLine[i];
			switch (c) {
				case '"' :
					isQuoted = !isQuoted;
					break;
				case ',' :
					if (isQuoted) {
						record += c;
					} else {
						// next record.
						results.push(record);
						record = '';
					}
					break;
				default :
					record += c;
			}
		}
		results.push(record);
		record = '';
		return results;
	},

	// Given the an entire csv file string or well ended chunk
	// convert to a rectangular 2d matrix.
	parse : function (csv) {
		var lines = csv.split('\n');
		var results = [];
		for (var i = 0; i < lines.length; i++) {
			// Mainly to make sure we skip empty lines and remove the \r from windows files.
			var line = dojoString.trim(lines[i]);
			if (line) {
				results.push( this.parseLine(lines[i]) );
			}
		}
		return results;
	},

	// Reads a chunk of a file.
	// NOTE: This may be in the middle of a line.
	// Returns a dojo/Deferred.
	readSlice : function (file, offsetStart, offsetEnd) {
		var deferred = new dojoDeferred();
		var blob = file.slice(offsetStart, offsetEnd);
		var reader = new FileReader();
		var _this = this;
		reader.onload = function (e) {
			var data = _this._ArrayBufferToString(reader.result);
			deferred.resolve(data);
		};
		reader.readAsArrayBuffer(blob);
		return deferred;
	},

	// Convert from the limited ArrayBuffer structure to a string.
	_ArrayBufferToString : function (arrayBuffer) {
		var typedArray = new Uint8Array(arrayBuffer);
		var max = typedArray.length;
		var arr = new Array(max);
		for (var i = 0; i < max; i++) {
			arr[i] = String.fromCharCode(typedArray[i]);
		}
		return arr.join('');
	}
};

return module;
});