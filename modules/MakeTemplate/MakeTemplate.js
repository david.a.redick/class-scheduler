// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// Displays high level schedule stats for an entire week.
// This view's main purpose is to allow the user to drill down to a specific day.
define([
	'dojo/_base/declare',
	'dijit/_Widget',
	'dijit/_TemplatedMixin',
	'../RoomTable/RoomTable',
	'dojo/dom-class',
	'dojo/text!./MakeTemplate.html'
],
function (dojoDeclare, dijitWidget, dijitTemplatedMixin, RoomTable, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'MakeTemplate.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidget, dijitTemplatedMixin], {
	// @Override
	templateString : templateString,
	inputName : null,
	containerRoomTable : null,

	// Must pass into via constructor.
	templatesData : null,

	// @Override
	postCreate : function () {
		var firstTemplateKey = Object.keys(this.templatesData)[0];

		this.roomTable = new RoomTable({
			templateData : this.templatesData[firstTemplateKey],
			showActions : true
		}).placeAt(this.containerRoomTable);
	},

	hide : function () {
		dojoDomClass.add(this.domNode, 'hidden');
		return this;
	},

	show : function () {
		dojoDomClass.remove(this.domNode, 'hidden');
		return this;
	}
});
});