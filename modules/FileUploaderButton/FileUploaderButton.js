// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A stylable button for the file uploader.
// Useful because many browsers create their own labels with generated temp file names.
// NOTE: Opera has a limitation that won't allow programmatic invoking of click of hidden input type=file objects.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'dojo/text!./FileUploaderButton.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'FileUploaderButton.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// @Override
	templateString : templateString,
	label: '',
	nodeLabel : null,
	nodeFileUploader : null,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
		this.nodeLabel.innerHTML = this.label;

		var _this = this;
		this.nodeFileUploader.onchange = function (e) {
			var file = _this.nodeFileUploader.files[0];
			_this.onFileSelect(file);
		};
	},

	show : function () {
	},

	hide : function () {
	},

	_onClick : function (e) {
		this.nodeFileUploader.click();
	},

	// Controller object should override.
	onFileSelect : function (file) {
		var nop = null;
	}
});
});