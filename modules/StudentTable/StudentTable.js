// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A roster of students enrolled and ready to be scheduled.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'./StudentRow',
	'../parserCsv/parserCsv',
	'../FileUploaderButton/FileUploaderButton',
	'./groupingLogic',
	'dojo/text!./StudentTable.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass, StudentRow, parserCsv, FileUploaderButton, groupingLogic, templateString) {

var myAlert = function (msg) {
	msg = 'StudentTable.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// @Override
	templateString : templateString,
	tableBody : null,
	rowPrompt : null,

	// An array of objects in the form:
	// name - String
	// grade - String Enum of: 'K', '1st', '2nd', '3rd', '4th' ... '8th'
	// school - String
	// gradeGroup - Optional. String Enum of: 'GK-3', 'G4-6', 'G7-8'
	// groupId - Optional. String.
	// id - String
	students : null,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
		var _this = this;
		new FileUploaderButton({
			label : 'Import CSV',
			onFileSelect : function (file) {
				_this._importCsvReplace(file);
			}
		}).placeAt(this.nodeImportContainer).startup();
	},

	hide : function () {
		dojoDomClass.add(this.domNode, 'hidden');
		return this;
	},

	show : function () {
		dojoDomClass.remove(this.domNode, 'hidden');
		return this;
	},

	onClickAddStudent : function () {
		var _this = this;
		this.row = new StudentRow({
			isEditMode : true,
			studentData : {
				name : ' ',
				grade : 'K',
				school : ' '
			},
			onClickCancel : function () {
				_this.row.destroyRecursive();
				_this.row = null;
			}
		});
		this.row.placeAt(this.tableBody, 'first');
	},

	onClickMakeGroups : function () {
		//groupingLogic.groupStudents(this.students);
		alert('make groups');
	},

	// Replace everything with the given file.
	_importCsvReplace : function (file) {
		var _this = this;

		parserCsv.readAndParseFile(file).then(function (data) {
			var hasHeader = false;
			var len = data.length;
			if ('Student Name' == data[0][0]) {
				// The first row is actually a header row.
				hasHeader = true;
				len--;
			}

			// Empty out the students array.
			_this.students.splice(0);

			data.some(function (c,i,a) {
				if (0 == i && hasHeader) {
					// Skip header
				} else {
					_this.students.push({
						name : c[0],
						grade : c[1],
						school : c[2],
						gradeGroup : groupingLogic.gradeToGradeGroup[c[1]],
						groupId : null,
						id : i
					});
				}
			});
			groupingLogic.groupStudents(_this.students);
			_this._buildTable();
		});
	},

	// Append the current data with the given file
	_importCsvAppend : function (file) {
		// TODO
	},

	_buildTable : function () {
		var _this = this;
		if (null == this.students || 0 == this.students.length) {
			// Show prompt.
			dojoDomClass.remove(this.rowPrompt, 'hidden');
		} else {
			// Remove prompt.
			dojoDomClass.add(this.rowPrompt, 'hidden');

			// Actual table building.
			this.students.some(function (c,i,a) {
				var row = new StudentRow({
					studentData : c
				});
				row.placeAt(_this.tableBody);
				return false;
			});
		}
	},

	onClickSortName : function () {
	},

	onClickSortGrade : function () {
	},

	onClickSortSchool : function () {
	}
});
});
