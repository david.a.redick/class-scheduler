// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// The row for a student data with an editable and read only view.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'dojo/text!./StudentRow.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'StudentRow.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// Constructor parameter
	// Fields:
	// name
	// grade
	// school
	studentData : null,
	isEditMode : false,

	// @Override
	templateString : templateString,
	nameReadOnly : null,
	nameEdit : null,
	nameInput : null,
	gradeReadOnly : null,
	gradeEdit : null,
	gradeSelect : null,
	schoolReadOnly : null,
	schoolEdit : null,
	schoolInput : null,
	iconEdit : null,
	iconSave : null,
	iconCancel : null,
	iconDelete : null,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	postCreate : function () {
		this.gradeSelect.value = this.studentData.grade;
		if (this.isEditMode) {
			this.toggleEditMode();
		}
	},

	toggleEditMode : function () {
		dojoDomClass.toggle(this.iconEdit, 'hidden');
		dojoDomClass.toggle(this.iconSave, 'hidden');
		dojoDomClass.toggle(this.iconCancel, 'hidden');
		dojoDomClass.toggle(this.iconDelete, 'hidden');

		dojoDomClass.toggle(this.nameReadOnly, 'hidden');
		dojoDomClass.toggle(this.nameEdit, 'hidden');

		dojoDomClass.toggle(this.gradeReadOnly, 'hidden');
		dojoDomClass.toggle(this.gradeEdit, 'hidden');

		dojoDomClass.toggle(this.schoolReadOnly, 'hidden');
		dojoDomClass.toggle(this.schoolEdit, 'hidden');
	},

	onClickEdit : function () {
		this.toggleEditMode();
	},

	onClickSave : function () {
		this.toggleEditMode();
	},

	onClickCancel : function () {
		this.toggleEditMode();
	},

	onClickDelete : function () {
		alert('StudentRow.onClickDelete');
	}
});
});