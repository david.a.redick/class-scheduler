// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

define(function () {

var module = {};

// Many grades are grouped into one grade group (id)
module.gradeToGradeGroup = {
	'K' : 'GK-3',
	'1st' : 'GK-3',
	'2nd' : 'GK-3',
	'3rd' : 'GK-3',
	'4th' : 'G4-6',
	'5th' : 'G4-6',
	'6th' : 'G4-6',
	'7th' : 'G7-8',
	'8th' : 'G7-8'
};

// Using map to prevent duplicates
module.gradeGroupIds = {};
for (var grade in module.gradeToGradeGroup) {
	module.gradeGroupIds[module.gradeToGradeGroup[grade]] = true;
}

module.makeGroupId = function (gradeGroup, counter) {
	return gradeGroup + '/' + counter;
};

// Group:
// studentIds - An array of strings
// groupId - String
// gradeGroup - String
module.makeGroup = function (groups, gradeGroup) {
	var counter = groups.length;
	groups.unshift({
		studentIds : [],
		groupId : module.makeGroupId(gradeGroup, counter),
		gradeGroup : gradeGroup
	});
};

// Grouping Data is mapping of grade group ids to an array of groups.
// The 0th index of the array (aka groups[0]) is a group that has space for more student(s).
// The other groups in the array are full.
// @see makeGroup
// @see groupStudents
module.initGroupingData = function () {
	var groupingData = {};

	for (var gradeGroup in module.gradeGroupIds) {
		groupingData[gradeGroup] = [];
		module.makeGroup(groupingData[gradeGroup], gradeGroup);
	}

	return groupingData;
};

// Assumes that groupingData needs to be created from scratch
module.groupStudents = function (roster) {
	var MAX_STUDENTS_IN_GROUP = 25;

	var groupingData = module.initGroupingData();

	roster.some(function (student, index, arr) {
		// Importing should automatically set the gradeGroup
		// but it may have been updated or deleted by the user.
		if (!student.gradeGroup) {
			student.gradeGroup = module.gradeToGradeGroup[student.grade];
		}

		var gradeGroup = student.gradeGroup;

		var groups = groupingData[gradeGroup];
		var groupToBeFilled = groups[0];

		// Cross reference the data structures
		groupToBeFilled.studentIds.push(student.id);
		student.groupId = groupToBeFilled.groupId;

		// If the working group is full then create another.
		if (MAX_STUDENTS_IN_GROUP <= groupToBeFilled.studentIds.length) {
			module.makeGroup(groups, gradeGroup);
		}
	});

	return groupingData;
};

return module;
});