// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// Displays high level schedule stats for an entire week.
// This view's main purpose is to allow the user to drill down to a specific day.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
// wrap
	'dojo/dom-attr',
	'dojo/dom-class',
	'dojo/text!./ViewWeek.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic,
 dojoDomAttr, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'ViewWeek.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// @Override
	templateString : templateString,

	strWeekDate : null,
	scheduledDays : null,

	hide : function () {
		dojoDomClass.add(this.domNode, 'hidden');
		return this;
	},

	show : function () {
		dojoDomClass.remove(this.domNode, 'hidden');
		return this;
	},

	onClickShowWeekPicker : function () {
		alert('Displays a drop down calendar to select a different week.');
	},

	onClickDetailedFullView : function () {
		window.location = './mock.detailed.view.html';
	},

	onClickDay : function (mouseEventClick) {
		var node = mouseEventClick.currentTarget;
		var dayOfWeek = dojoDomAttr.get(node, 'data-class-scheduler-day');
		switch (dayOfWeek) {
			case 'monday' :
			case 'tuesday' :
			case 'wednesday' :
			case 'thursday' :
				window.location = '../ViewDay/develop.html';
				break;
			case 'friday' :
				window.location = '../MakeSchedule/develop.html?scheduleFor=day';
				break;
			case 'saturday' :
			case 'sunday' :
				window.location = '../ViewDay/develop.html';
				break;
			default:
				myAlert('unhandled dayOfWeek = ' + dayOfWeek);
		}
	}
});
});
