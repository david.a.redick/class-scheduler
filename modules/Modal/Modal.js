// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// A modal / pop up dialog container.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'dojo/text!./Modal.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass, templateString) {

var myAlert = function (msg) {
	msg = 'Modal.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	// @Override
	templateString : templateString,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
	},

	show : function () {
		dojoDomClass.remove(this.domNode, 'hidden');
	},

	hide : function () {
		dojoDomClass.add(this.domNode, 'hidden');
	},

	_rootClose : function () {
		this.hide();
		this.onClose();
	},

	// Call back for controllers when user has closed (after hide) the modal.
	onClose : function () {
	},

	// If user clicks on the container or its children, don't continue to the root close function.
	_stopClick : function (e) {
		e.stopPropagation();
	}
});
});