// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// The form to edit a class time block.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'dojox/dtl/_DomTemplated',
	'dojox/dtl/tag/logic',
	'dojo/dom-class',
	'../RoomTable/timeUtils',
	'dojo/text!./EditClassTimeBlock.html'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, dojoxDjangoDomTemplated, dojoxDjangoTagLogic, dojoDomClass,
 timeUtils, templateString) {

var myAlert = function (msg) {
	msg = 'EditClassTimeBlock.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([dijitWidgetBase, dojoxDjangoDomTemplated], {
	timeBlock : null,

	// @Override
	templateString : templateString,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
		this.selectDuration.value = this.timeBlock.lengthMins;
	},

	onClickDelete : function () {
		myAlert('controller should override');
	},

	onClickCancel : function () {
		myAlert('controller should override');
	},

	_onClickSave : function () {
		var startTime = this.editStartTime.value;
		var updatedTimeBlock = timeUtils.normalizeTimeBlock({
			teacher : this.editTeacher.value,
			startTime : startTime,
			endMins : timeUtils.time24hToMinsFromMidnight(startTime) + parseInt(this.selectDuration.value)
		});
		this.onClickSave(updatedTimeBlock);
	},

	onClickSave : function (updatedTimeBlock) {
		myAlert('controller should override');
	}
});
});