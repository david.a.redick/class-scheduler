// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// The form to edit a class time block.
define([
	'dojo/_base/declare',
	'dojo/_base/config',
	'dijit/_WidgetBase',
	'./Modal',
	'./EditClassTimeBlock'
],
function (dojoDeclare, dojoConfig, dijitWidgetBase, Modal, EditClassTimeBlock) {

var myAlert = function (msg) {
	msg = 'ModalEditClassTimeBlock.js - ' + msg;
	console.log(msg);
	alert(msg);
	throw msg;
};

return dojoDeclare([Modal], {
	timeBlock : null,

	// @Override
	constructor : function (args) {
		var nop = null;
	},

	// @Override
	postCreate : function () {
		var _this = this;
		new EditClassTimeBlock({
			timeBlock : this.timeBlock,
			onClickDelete : function () {
				_this.onClickDelete();
			},
			onClickCancel : function () {
				_this.hide();
				_this.onClickCancel();
			},
			onClickSave : function (updatedTimeBlock) {
				_this.onClickSave(updatedTimeBlock);
			}
		}).placeAt(this.container);
	},

	// @Override
	onClose : function () {
		myAlert('Errrrrr-onClose');
	},

	onClickDelete : function () {
		myAlert('Errrrrr-onClickDelete');
	},

	onClickCancel : function () {
		myAlert('Errrrrr-onClickCancel');
	},

	onClickSave : function (updatedTimeBlock) {
		myAlert('Errrrrr-onClickSave');
	}
});
});