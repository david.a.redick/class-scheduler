// Copyright (C) 2017 - David A. Redick david.a.redick@gmail.com
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
// 
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.


// See - http://dojotoolkit.org/documentation/tutorials/1.12/modules_advanced/index.html
{
var rootContext = 'https://david-redick.github.io/class-scheduler';

window.dojoConfig = {
	async : true,

	// The version number of the application.
	cacheBust : 'version0',

	packages: [
		{ name: 'classScheduler', location: rootContext+'/modules/' }
	],

	rootContext : rootContext
};
}
